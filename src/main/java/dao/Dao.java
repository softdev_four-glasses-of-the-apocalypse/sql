
package dao;

import java.util.List;

public interface Dao<T> {
    T get(int id);
    List<T> getAll();
    T save(T obj);
    T update(T obj);
    int delete(T obj); 
    List<T> getAll(String where, String order);
}
