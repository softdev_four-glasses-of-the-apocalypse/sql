package com.mycompany.dbproject;

import dao.UserDao;
import helper.DatabaseHelper;
import model.User;

public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for(User u: userDao.getAll()) {
            System.out.println(u);
        }
        for(User u: userDao.getAll(" user_name like 'u%' ", " user_name asc, user_gender desc ")) {
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
