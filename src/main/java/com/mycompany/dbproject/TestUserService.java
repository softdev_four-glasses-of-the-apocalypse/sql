package com.mycompany.dbproject;

import model.User;
import service.UserService;

public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user2", "password");
        if(user!=null) {
            System.out.println("Welcome user :  " + user.getName());
        } else {
            System.out.println("Error");
        }
    }
}
